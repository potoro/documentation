# Report 1 diagrams

## View source diagrams

You can review source diagrams in <b>src</b> folder. Which includes all the
diagram.io files we use for designed.

Be notice that, some src file to editting of some diagram will be private and not shown for
global user, but you can still review the full resolution of exported images

## View raw images

You can quickly view raw images which are render from source diagrams in <b>raw</b> folder. Which includes all the HD images of quick capture
diagram.io files we use for designed.

## Contribute your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:b6a85ae1b8f661776714517ceb5d4ff3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:b6a85ae1b8f661776714517ceb5d4ff3?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:b6a85ae1b8f661776714517ceb5d4ff3?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/potoro/documentation.git
git branch -M main
git push -uf origin main
```

We - our developer team with access will review your pull request

## License

Potoro (☞ ﾟヮﾟ)☞ had full permision on this product. No copyright
