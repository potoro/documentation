# Install App

## #System Requirements

Bare-metal Server

- CPU: Intel Core I5 2.5GHz 4 cores
- RAM: 4GB
- Storage: 200GB
- OS: Ubuntu 18+
  
VPS (minimal)

- CPU: 2 cores
- RAM: 4GB
- Storage: 20GB SSD
- OS: Ubuntu 18+
  
VPS (recommended)

- CPU: 3 cores
- RAM: 6GB
- Storage: 40GB SSD
- OS: Ubuntu 18+
  
Required: docker v20+, docker compose v2+ (or Kubernetes v1.9 if you using K8S)

## #Install the following tools

- [Docker v20+](https://docs.docker.com/engine/install/) (required)
- [Docker compose v2+](https://docs.docker.com/compose/install/) (required)
- [Nginx](https://docs.nginx.com/nginx/admin-guide/installing-nginx/installing-nginx-open-source/)

## #Run App

Service images from Docker Hub:

- huynx11/potoro-fe-user:latest
- huynx11/potoro-fe-admin:latest
- huynx11/potoro-be-api-gateway:latest
- huynx11/potoro-be-identity-service:latest
- huynx11/potoro-be-communicate-service:latest
- huynx11/potoro-be-payment-service:latest
- huynx11/potoro-be-facility-service:latest
- huynx11/potoro-be-search-service:latest
- huynx11/potoro-be-envoice-service:latest
- huynx11/potoro-be-communicate-service:latest
- huynx11/potoro-be-booking-service:latest
- huynx11/potoro-be-health-check-service:latest

You can run each Service with each image from Docker Hub with steps:

\#Way one - Use our available files

You just need to run the file 'docker-compose.yml' in this repo. We recommend this method because all necessary configurations are already setup. Following these steps:

- Change YOUR_CONNECTION_STRING
- Run command: docker-compose up -d

\#Way two: Using Docker Command

- docker run --name \<container_name\> -d -p \<port_number\> \<image_name\>

\#Way three: Using Docker Compose

- Create file 'docker-compose.yml' for each service with template:

```yml:
version: "3.8"

services:
 payment-service: #service name
    container_name: payment-service #container name
    image: huynx11/potoro-be-payment-service:latest #image name
    restart: unless-stopped
    environment:
    - ASPNETCORE_ENVIRONMENT=Development
    - ConnectionStrings_DefaultConnection=<YOUR_CONNECTION_STRING>
    ports:
      - "5225:80" #port number
```

- Run command: docker-compose up -d

\#After running the above services. You need to run the following applications with the above steps:

\#**If you use way one. Please skip this step**

- portainer/portainer-ce:latest
- prom/prometheus:latest
- grafana/grafana-oss:latest
- quay.io/prometheus/node-exporter:latest
- google/cadvisor:latest
- docker.elastic.co/elasticsearch/elasticsearch:7.16.1
- docker.elastic.co/kibana/kibana:7.16.1
- rabbitmq:3-management
- telegraf
- influxdb:1.8-alpine

## **IMPORTANT**

### Config Variable

\#Move all files in folder 'config' to the folder is running the corresponding docker-compose.yml file.

\#Monitoring

Steps:

- cd /etc/

- mkdir prometheus

- cp prometheus.yml /etc/prometheus/

\#If you do not using Way One config following config:

```yml:
  - job_name: Name_Metric
    scheme: https # or http
    static_configs:
    targets: ['YOUR_END_POINT']
```

\#Set up Jenkins for CICD

Run script in your server:

```sh:
#!/usr/bin/env bash

help() {
  cat <<EOF
  Arguments:
  +\$1 given username
  Usage example:
  $ ./add-docker-user.sh jenkins
EOF
}

# init vars
USR=$1
if [[ -z $USR ]]; then
  help
  exit 1
fi

# generate SSH key pairs
# REF. https://stackoverflow.com/a/43235320/1235074
ssh-keygen -q -N '' -m PEM -t rsa -f "$HOME/.ssh/id_rsa_$USR" <<< ""$'\n'"y" 2>&1 >/dev/null

# create new user
useradd -m -d /home/$USR -s /bin/bash $USR
usermod -aG docker $USR
mkdir /home/$USR/.ssh
touch /home/$USR/.ssh/authorized_keys
cat "$HOME/.ssh/id_rsa_$USR.pub" > /home/$USR/.ssh/authorized_keys
ssh -i $HOME/.ssh/id_rsa_$USR $USR@localhost "docker --version && echo '>>> DONE. New user added'"
```

Run commands:

- su jenkins
- mkdir data
- docker run -v /var/run/docker.sock:/var/run/docker.sock -v \$(which docker):$(which docker) -v `pwd`/data:/var/jenkins_home  -p 8080:8080  --user 1000:998 --name jenkins-server -d jenkins/jenkins:lts
- docker exec -ti jenkins-server /bin/bash -c "cat /var/jenkins_home/secrets/initialAdminPassword"
  
Use password appear on screen to login Jenkins.

Next step: follwing this docs to config Jenkins with GitLab: [Install Jenkins with GitLab](https://docs.gitlab.com/ee/integration/jenkins.html)

Finally: go to in your Server create each repo folder:

Exmaple: In search-service repo

- vi deploy.sh

```sh:
cd `dirname $BASH_SOURCE`

docker-compose stop
docker-compose rm -f
docker-compose pull
docker-compose up -d

cd - > /dev/null
```

- chmod +x deploy.sh
- vi docker-compose.yml

```yml:
version: "3.8"

services:
 search-service: #service name
    container_name: payment-service #container name
    image: huynx11/potoro-be-search-service:latest #image name
    restart: unless-stopped
    environment:
    - ASPNETCORE_ENVIRONMENT=Development
    - ConnectionStrings_DefaultConnection=<YOUR_CONNECTION_STRING>
    ports:
      - "5225:80" #port number
```

## Public app to internet using Nginx

Steps:

```conf:
1. sudo apt update
2. sudo apt install nginx
3. sudo ufw allow 'Nginx Full'
4. sudo nginx

#Open multi server on 1 IP
5. cd /etc/nginx/sites-enabled/
6. unlink default
7. sudo nano default_server
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /var/www/html;
        index index.html index.htm index.nginx-debian.html;
        server_name _;
        location / {
            try_files $uri $uri/ =404;
        }
}
8. cd /etc/nginx/conf.d
9. sudo nano new_sites.conf
server {
        listen        80;
        server_name   your-aap.com.vn; #Change your domain
        client_max_body_size 900M;
        location / {
                proxy_pass         http://127.0.0.1:5001;#Change port your app is running
                proxy_http_version 1.1;
                proxy_set_header   Upgrade $http_upgrade;
                proxy_set_header   Connection keep-alive;
                proxy_set_header   Host $host;
                proxy_cache_bypass $http_upgrade;
                proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Proto $scheme;
        }
}
10. nginx -t
11. cd ~
12. systemctl reload nginx or service nginx restart
```

## Default authenticate all app

- username: admin
- password: admin

## Using Kubernetes and LoadBalancer

Required:

- 1+ Cluster + 1+ Node
- CPU: 2 cores
- RAM: 4GB
- Storage: 20GB

Steps:

- kubectl apply -f deployment-config.yml
- kubectl apply -f auto-scale-config.yml
- kubectl apply -f load-balancer-config.yml

## If you have any problem. Please contact us [Potoro.vn](https://www.potoro.vn/)
